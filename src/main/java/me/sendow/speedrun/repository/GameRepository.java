package me.sendow.speedrun.repository;

import me.sendow.speedrun.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GameRepository extends JpaRepository<Game, Long> {

    @Query("select c.game from Category c where c.id = :categoryId")
    Game findByCategoryId(@Param("categoryId") long id);

}
