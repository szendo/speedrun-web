package me.sendow.speedrun.repository;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntryRepository extends JpaRepository<Entry, Long> {

    List<Entry> findByCategoryOrderByRunTime(Category category);

    @Query("select count(e) + 1 as rank from Entry e where e.category = :category and e.runTime < :runTime")
    long getRankForEntry(@Param("category") Category category, @Param("runTime") long runTime);

    Entry findByCategoryAndUsername(Category category, String username);

}
