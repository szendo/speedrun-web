package me.sendow.speedrun.repository;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByGameOrderByName(Game game);

    @Query("select e.category from Entry e where e.id = :entryId")
    Category findByEntryId(@Param("entryId") long id);

}
