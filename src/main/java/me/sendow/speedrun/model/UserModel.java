package me.sendow.speedrun.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserModel {
    @NotNull
    @Size(min = 1, max = 255)
    private String username;
    @NotNull
    @Size(min = 1, max = 1024)
    private String password;

    public UserModel() {
    }

    public UserModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
