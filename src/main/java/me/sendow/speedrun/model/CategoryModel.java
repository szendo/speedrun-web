package me.sendow.speedrun.model;


import me.sendow.speedrun.entity.Category;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryModel extends AbstractModelBase<Category> {

    @NotNull
    @Size(min = 1, message = "this field cannot be blank")
    private String name;

    @NotNull
    private String rules;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    @Override
    public Category toEntity() {
        final Category category = new Category();
        category.setName(getName());
        category.setRules(getRules());
        return category;
    }

}
