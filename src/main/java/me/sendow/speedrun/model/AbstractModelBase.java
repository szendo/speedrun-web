package me.sendow.speedrun.model;

public abstract class AbstractModelBase<T> {

    public abstract T toEntity();

}
