package me.sendow.speedrun.model;

import me.sendow.speedrun.entity.Entry;

public class EntryModel extends AbstractModelBase<Entry> {

    private String username;

    private int runTimeH;
    private int runTimeM;
    private int runTimeS;
    private int runTimeC;

    private String videoUrl;
    private String comment;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRunTimeH() {
        return runTimeH;
    }

    public void setRunTimeH(int runTimeH) {
        this.runTimeH = runTimeH;
    }

    public int getRunTimeM() {
        return runTimeM;
    }

    public void setRunTimeM(int runTimeM) {
        this.runTimeM = runTimeM;
    }

    public int getRunTimeS() {
        return runTimeS;
    }

    public void setRunTimeS(int runTimeS) {
        this.runTimeS = runTimeS;
    }

    public int getRunTimeC() {
        return runTimeC;
    }

    public void setRunTimeC(int runTimeC) {
        this.runTimeC = runTimeC;
    }

    public long getComputedRunTime() {
        return 10 * (runTimeC + 100 * (runTimeS + 60 * (runTimeM + 60 * runTimeH)));
    }

    public void setComputedRunTime(long runTime) {
        this.runTimeH = (int) (runTime / (1000 * 60 * 60));
        this.runTimeM = (int) (runTime / (1000 * 60) % 60);
        this.runTimeS = (int) (runTime / 1000 % 60);
        this.runTimeC = (int) (runTime / 10 % 100);
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public Entry toEntity() {
        final Entry entry = new Entry();
        entry.setUsername(getUsername());
        entry.setRunTime(getComputedRunTime());
        entry.setVideoUrl(getVideoUrl());
        entry.setComment(getComment());
        return entry;
    }

    @Override
    public String toString() {
        return "EntryModel{" +
                "username='" + username + '\'' +
                ", runTimeH=" + runTimeH +
                ", runTimeM=" + runTimeM +
                ", runTimeS=" + runTimeS +
                ", runTimeC=" + runTimeC +
                ", videoUrl='" + videoUrl + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
