package me.sendow.speedrun.controller;

import me.sendow.speedrun.model.UserModel;
import me.sendow.speedrun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(@ModelAttribute("user") UserModel model) {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute("user") @Valid UserModel model, BindingResult result) {
        if (result.hasErrors()) {
            return "register";
        }
        try {
            userService.createUser(model);
        } catch (DataIntegrityViolationException e) {
            result.addError(new FieldError("user", "username", "username already in use: " + model.getUsername()));
            return "register";
        }
        return "redirect:/";
    }
}


