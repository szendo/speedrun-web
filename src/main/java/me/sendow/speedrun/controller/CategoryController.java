package me.sendow.speedrun.controller;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.CategoryModel;
import me.sendow.speedrun.model.EntryModel;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;
import me.sendow.speedrun.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private GameService gameService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EntryService entryService;

    @RequestMapping(value = "/{categoryId}/edit", method = RequestMethod.GET)
    public String showEditCategoryForm(@ModelAttribute("model") CategoryModel model,
                                       @PathVariable("categoryId") long id,
                                       ModelMap modelMap) {
        final Category category = categoryService.findById(id);
        model.setName(category.getName());
        model.setRules(category.getRules());
        modelMap.addAttribute("category", category);
        return "category/edit";
    }

    @RequestMapping(value = "/{categoryId}/edit", method = RequestMethod.POST)
    public String processEditCategoryForm(@ModelAttribute("model") @Valid CategoryModel model, BindingResult result,
                                          @PathVariable("categoryId") long id,
                                          ModelMap modelMap) {
        final Category category = categoryService.findById(id);
        if (result.hasErrors()) {
            modelMap.addAttribute("category", category);
            return "category/edit";
        }
        categoryService.update(id, model);

        return "redirect:/game/" + category.getGame().getId() + "/category/" + category.getId();
    }

    @RequestMapping(value = "/{categoryId}/delete", method = RequestMethod.POST)
    public String deleteCategory(@PathVariable("categoryId") long id) {
        final Game game = gameService.findByCategoryId(id);
        categoryService.delete(id);
        return "redirect:/game/" + game.getId();
    }


    @RequestMapping(value = "/{categoryId}/editEntry", method = RequestMethod.GET)
    public String showEditEntryForm(@ModelAttribute("model") EntryModel model,
                                    @PathVariable("categoryId") long id,
                                    Principal principal,
                                    ModelMap modelMap) {
        final String username = (principal != null) ? principal.getName() : null;
        final Entry entry = entryService.findForPlayerInCategory(id, username);
        if (entry != null) {
            model.setComputedRunTime(entry.getRunTime());
            model.setVideoUrl(entry.getVideoUrl());
            model.setComment(entry.getComment());
            modelMap.addAttribute("category", entry.getCategory());
        } else {
            modelMap.addAttribute("category", categoryService.findById(id));
        }
        return "entry/edit";
    }

    @RequestMapping(value = "/{categoryId}/editEntry", method = RequestMethod.POST)
    public String processEditEntryForm(@ModelAttribute("model") @Valid EntryModel model, BindingResult result,
                                       @PathVariable("categoryId") long id,
                                       Principal principal,
                                       ModelMap modelMap) {
        if (result.hasErrors()) {
            final Category category = categoryService.findById(id);
            modelMap.addAttribute("category", category);
            return "entry/edit";
        }
        final String username = (principal != null) ? principal.getName() : null;
        final Entry entry = entryService.findForPlayerInCategory(id, username);
        final Category category;
        if (entry != null) {
            entryService.update(entry.getId(), model);
            category = entry.getCategory();
        } else {
            model.setUsername(username);
            final Entry newEntry = entryService.createForCategory(model.toEntity(), id);
            category = newEntry.getCategory();
        }
        return "redirect:/game/" + category.getGame().getId() + "/category/" + category.getId();
    }

}
