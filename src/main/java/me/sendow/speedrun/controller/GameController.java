package me.sendow.speedrun.controller;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.CategoryModel;
import me.sendow.speedrun.model.GameModel;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;
import me.sendow.speedrun.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EntryService entryService;

    @RequestMapping(method = RequestMethod.GET)
    public String listGames(ModelMap model,
                            @RequestHeader(value = "X-Requested-With", required = false) String requestedWith,
                            @PageableDefault(value = 10) Pageable page) {
        model.addAttribute("games", gameService.getGames(page));

        if ("XMLHttpRequest".equals(requestedWith)) {
            return "index :: gameList";
        }
        return "index";
    }

    @RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
    public String showGame(@PathVariable("gameId") Long gameId,
                           ModelMap modelMap) {
        modelMap.addAttribute("game", gameService.findById(gameId));
        modelMap.addAttribute("categories", categoryService.findCategoriesForGameId(gameId));
        return "game";
    }

    @RequestMapping(value = "/{gameId}/category/{categoryId}", method = RequestMethod.GET)
    public String showGameWithCategory(@PathVariable("gameId") Long gameId,
                                       @PathVariable("categoryId") Long categoryId,
                                       ModelMap modelMap) {
        final Category category = categoryService.findById(categoryId);
        if (category == null) {
            return "redirect:/" + gameId;
        }

        modelMap.addAttribute("game", gameService.findById(gameId));
        modelMap.addAttribute("categories", categoryService.findCategoriesForGameId(gameId));
        modelMap.addAttribute("category", category);
        modelMap.addAttribute("entries", entryService.getEntriesForCategory(category));
        return "game";
    }

    @RequestMapping(value = "/{gameId}/image", method = RequestMethod.GET, produces = "image/png")
    @ResponseBody
    public byte[] showGameImage(@PathVariable("gameId") long id) {
        return gameService.findById(id).getImage();
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewGameForm(@ModelAttribute("model") GameModel model) {
        return "game/new";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String processNewGameForm(@ModelAttribute("model") @Valid GameModel model, BindingResult result) {
        final byte[] imageData = model.getImageData();
        if (imageData == null || imageData.length == 0) {
            result.addError(new FieldError("model", "image", "invalid or missing image"));
        }
        if (result.hasErrors()) {
            return "game/new";
        }
        final Game game = gameService.create(model.toEntity());

        return "redirect:/game/" + game.getId();
    }

    @RequestMapping(value = "/{gameId}/edit", method = RequestMethod.GET)
    public String showEditGameForm(@ModelAttribute("model") GameModel model,
                                   @PathVariable("gameId") long id,
                                   ModelMap modelMap) {
        final Game game = gameService.findById(id);
        model.setTitle(game.getTitle());
        modelMap.addAttribute("game", game);
        return "game/edit";
    }

    @RequestMapping(value = "/{gameId}/edit", method = RequestMethod.POST)
    public String processEditGameForm(@ModelAttribute("model") @Valid GameModel model, BindingResult result,
                                      @PathVariable("gameId") long id,
                                      ModelMap modelMap) {
        final byte[] imageData = model.getImageData();
        if (imageData != null && imageData.length == 0) {
            result.addError(new FieldError("model", "image", "invalid image"));
        }
        if (result.hasErrors()) {
            modelMap.addAttribute("game", gameService.findById(id));
            return "game/edit";
        }
        gameService.update(id, model);

        return "redirect:/game/" + id;
    }

    @RequestMapping(value = "/{gameId}/delete", method = RequestMethod.POST)
    public String deleteGame(@PathVariable("gameId") long id) {
        gameService.delete(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/{gameId}/newCategory", method = RequestMethod.GET)
    public String showNewCategoryForm(@ModelAttribute("model") CategoryModel model,
                                      @PathVariable("gameId") long id,
                                      ModelMap modelMap) {
        final Game game = gameService.findById(id);
        modelMap.addAttribute("game", game);
        return "category/new";
    }

    @RequestMapping(value = "/{gameId}/newCategory", method = RequestMethod.POST)
    public String processNewCategoryForm(@ModelAttribute("model") @Valid CategoryModel model, BindingResult result,
                                         @PathVariable("gameId") long id,
                                         ModelMap modelMap) {
        final Game game = gameService.findById(id);
        if (result.hasErrors()) {
            modelMap.addAttribute("game", game);
            return "category/new";
        }
        final Category category = categoryService.createForGame(model.toEntity(), id);

        return "redirect:/game/" + id + "/category/" + category.getId();
    }

}
