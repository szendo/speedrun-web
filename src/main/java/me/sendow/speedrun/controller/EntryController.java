package me.sendow.speedrun.controller;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/entry")
public class EntryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EntryService entryService;

    @RequestMapping(value = "/{entryId}", method = RequestMethod.GET)
    public String showEntry(@PathVariable("entryId") long id,
                            @RequestHeader(value = "X-Requested-With", required = false) String requestedWith,
                            ModelMap model) {
        final Entry entry = entryService.findById(id);
        if (entry == null) {
            return "redirect:/";
        }

        model.addAttribute("entry", entry);
        model.addAttribute("entryRank", entryService.getRankForEntry(entry));

        if ("XMLHttpRequest".equals(requestedWith)) {
            return "entry/view :: entryDialog";
        }
        return "entry/view";
    }

    @RequestMapping(value = "/{entryId}/delete", method = RequestMethod.POST)
    public String deleteEntry(@PathVariable("entryId") long id) {
        final Category category = categoryService.findByEntryId(id);
        entryService.delete(id);
        return "redirect:/game/" + category.getGame().getId() + "/category/" + category.getId();
    }

}
