package me.sendow.speedrun.util;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public final class VideoEmbedUtil {

    private static final List<Provider> providers = Arrays.asList(
            new Provider(Pattern.compile("https?://www.youtube.com/watch\\?v=([^&]+)"),
                    "https://www.youtube.com/embed/$1"),
            new Provider(Pattern.compile("https?://www.twitch.tv/[^/]+/v/(\\d+)"),
                    "http://player.twitch.tv/?autoplay=false&video=v$1"));

    private VideoEmbedUtil() {
    }

    public static String getEmbedUrl(String url) {
        if (url == null) {
            return null;
        }

        for (Provider provider : providers) {
            final Matcher matcher = provider.urlPattern.matcher(url);
            if (matcher.matches()) {
                return matcher.replaceFirst(provider.embedUrl);
            }
        }

        return url;
    }

    private static class Provider {
        final Pattern urlPattern;
        final String embedUrl;

        Provider(Pattern urlPattern, String embedUrl) {
            this.urlPattern = urlPattern;
            this.embedUrl = embedUrl;
        }
    }
}
