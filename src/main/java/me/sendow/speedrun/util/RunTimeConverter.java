package me.sendow.speedrun.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RunTimeConverter implements Converter<Long, String> {

    @Override
    public String convert(Long value) {
        if (value != null) {
            StringBuilder result = new StringBuilder();

            if (value >= 1000 * 60 * 60) {
                result.append(String.format("%d:%02d:%02d",
                        value / (1000 * 60 * 60),
                        value / (1000 * 60) % 60,
                        value / 1000 % 60));
            } else {
                result.append(String.format("%d:%02d",
                        value / (1000 * 60),
                        value / 1000 % 60));
            }

            if (value / 10 % 100 != 0) {
                result.append(String.format(".%02d",
                        value / 10 % 100));
            }

            return result.toString();
        }
        return null;
    }

}
