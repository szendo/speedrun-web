package me.sendow.speedrun.service;

import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.GameModel;
import me.sendow.speedrun.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
@PreAuthorize("denyAll")
public class GameService {

    private static final Sort TITLE_SORT = new Sort("title");

    @Autowired
    private GameRepository gameRepository;

    @PreAuthorize("permitAll")
    public Page<Game> getGames(Pageable pageable) {
        Pageable pageableOrderByTitle = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), TITLE_SORT);
        return gameRepository.findAll(pageableOrderByTitle);
    }

    @PreAuthorize("permitAll")
    public Game findById(long id) {
        return gameRepository.findOne(id);
    }

    @PreAuthorize("permitAll")
    public Game findByCategoryId(long categoryId) {
        return gameRepository.findByCategoryId(categoryId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    public Game create(Game game) {
        return gameRepository.save(game);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void update(long id, GameModel model) {
        final Game game = gameRepository.findOne(id);
        game.setTitle(model.getTitle());
        final byte[] imageData = model.getImageData();
        if (imageData != null) {
            game.setImage(imageData);
        }
        gameRepository.save(game);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void delete(long id) {
        gameRepository.delete(id);
    }

}
