package me.sendow.speedrun.service;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.model.EntryModel;
import me.sendow.speedrun.repository.CategoryRepository;
import me.sendow.speedrun.repository.EntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@PreAuthorize("denyAll")
public class EntryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private EntryRepository entryRepository;

    @PreAuthorize("permitAll")
    public List<Entry> getEntriesForCategory(Category category) {
        return entryRepository.findByCategoryOrderByRunTime(category);
    }

    @PreAuthorize("permitAll")
    public long getRankForEntry(Entry entry) {
        return entryRepository.getRankForEntry(entry.getCategory(), entry.getRunTime());
    }

    @PreAuthorize("permitAll")
    public Entry findById(long id) {
        return entryRepository.findOne(id);
    }

    @PreAuthorize("isFullyAuthenticated()")
    public Entry findForPlayerInCategory(long categoryId, String username) {
        final Category category = categoryRepository.findOne(categoryId);
        return entryRepository.findByCategoryAndUsername(category, username);
    }

    @PreAuthorize("isFullyAuthenticated()")
    public Entry createForCategory(Entry entry, long categoryId) {
        final Category category = categoryRepository.findOne(categoryId);
        entry.setCategory(category);
        return entryRepository.save(entry);
    }

    @PreAuthorize("isFullyAuthenticated()")
    public void update(long id, EntryModel model) {
        final Entry entry = entryRepository.findOne(id);
        entry.setRunTime(model.getComputedRunTime());
        entry.setVideoUrl(model.getVideoUrl());
        entry.setComment(model.getComment());
        entryRepository.save(entry);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void delete(long id) {
        entryRepository.delete(id);
    }

}
