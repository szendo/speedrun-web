package me.sendow.speedrun.service;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.CategoryModel;
import me.sendow.speedrun.repository.CategoryRepository;
import me.sendow.speedrun.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@PreAuthorize("denyAll")
public class CategoryService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @PreAuthorize("permitAll")
    public List<Category> findCategoriesForGameId(long id) {
        final Game game = gameRepository.findOne(id);
        return categoryRepository.findByGameOrderByName(game);
    }

    @PreAuthorize("permitAll")
    public Category findById(long id) {
        return categoryRepository.findOne(id);
    }

    @PreAuthorize("permitAll")
    public Category findByEntryId(long entryId) {
        return categoryRepository.findByEntryId(entryId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    public Category createForGame(Category category, long gameId) {
        final Game game = gameRepository.findOne(gameId);
        category.setGame(game);
        return categoryRepository.save(category);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void update(long id, CategoryModel model) {
        final Category category = categoryRepository.findOne(id);
        category.setName(model.getName());
        category.setRules(model.getRules());
        categoryRepository.save(category);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void delete(long id) {
        categoryRepository.delete(id);
    }

}
