$(function () {

    $('a[href][data-ajax-target]').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        $($this.data('ajaxTarget')).load($this.attr('href'), function () {
            var $dialog = $(this);
            $dialog.parent()
                .modal('show')
                .one('hidden.bs.modal', function () {
                    $dialog.empty();
                });
        });
    });

});