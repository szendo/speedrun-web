$(function () {

    $('a[href][data-ajax-target]').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $li = $this.parent();
        history.replaceState(null, '', $this.attr('href'));
        $li.addClass('active')
            .siblings().removeClass('active');
        $($this.data('ajaxTarget')).load($this.attr('href'));
    });

});